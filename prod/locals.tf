# root/locals.tf

locals {
  vpc_cidr = "11.11.0.0/16"
}

locals {
  vpc = {
    public_subnet_count  = 2
    private_subnet_count = 3
    max_subnets          = 20
    public_cidrs         = [for i in range(2, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
    private_cidrs        = [for i in range(1, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
    create_natgw         = false
  }
}
