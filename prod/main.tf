# root/main.tf

module "vpc" {
  source               = "git::https://gitlab.com/mosar-infra/tf-module-vpc.git?ref=tags/v1.0.1"
  public_subnet_count  = local.vpc.public_subnet_count
  private_subnet_count = local.vpc.private_subnet_count
  vpc_cidr             = local.vpc_cidr
  max_subnets          = local.vpc.max_subnets
  public_cidrs         = local.vpc.public_cidrs
  private_cidrs        = local.vpc.private_cidrs
  create_natgw         = local.vpc.create_natgw
  environment          = "prod"
  managed_by           = "vpc"
}
